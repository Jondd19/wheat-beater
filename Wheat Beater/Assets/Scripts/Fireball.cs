﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    public Transform target;

    public float speed;


   

    void Start()
    {

       
            target = GameObject.Find("Egg").transform;

            speed = 5;
        


    }


    private void OnTriggerEnter(Collider other)
    {
        if (other == GameObject.Find("Player").GetComponent<CapsuleCollider>())
        {
            //GameObject.Find("Egg").GetComponent<Egghealth>().PlayerHealth -= 5;
            GameManager.Get().PlayerHealth -= 5;
            Destroy(this.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (target != null)
        {
            if (HasReachedTarget())
            {
                speed = 0;
                //GameObject.Find("Egg").GetComponent<Egghealth>().egghealth--;
                GameManager.Get().egghealth--;
                Destroy(this.gameObject);
            }
            UpdateMovement();
        }


    }




    bool HasReachedTarget()
    {
        float dist = Vector3.Distance(transform.position, target.position);
        return dist <= .01;
    }

    void UpdateMovement()
    {
        if (speed > 0)
        {
            Vector3 pos = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
            transform.position = pos;

            Vector3 dir = (target.position) - transform.position;
            if (dir.magnitude > 0)
            {
                Quaternion toRotation = Quaternion.LookRotation(dir);
                transform.rotation = Quaternion.Lerp(transform.rotation, toRotation, 0.1f);
            }
        }
    }
}
