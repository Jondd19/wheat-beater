﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class PauseMenuAndre : MonoBehaviour {

    public static bool isPaused = false;
    public GameObject PauseMenuUI;
    public AudioSource PauseBGM;
    public AudioSource LevelBGM;
    public AudioMixer mixer;


    void Start()
    {
        Time.timeScale = 1.0f;
        //Cursor.visible = false;
    }

    // Update is called once per frame
    void Update () {
		if(Input.GetKeyDown(KeyCode.P))
        {
            if(isPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
        
    }

    public void Pause()
    {
        PauseMenuUI.SetActive(true);
        Cursor.visible = true;
        Time.timeScale = 0f;
        PauseBGM.Play();
        LevelBGM.Pause();
        isPaused = true;
    }

    public void Resume()
    {
        PauseMenuUI.SetActive(false);
        Cursor.visible = false;
        Time.timeScale = 1f;
        PauseBGM.Stop();
        LevelBGM.UnPause();
        isPaused = false;
    }

    public void LoadMenu()
    {
        LevelLoader.Get().LoadLevel(0);
    }

    public void QuitGame()
    {
        LevelLoader.Get().QuitGame();
    }

    public void SetMasterVolume(float volume)
    {
        float dbVolume = Mathf.Log10(volume) * 20;
        if (volume == 0.0f)
        {
            dbVolume = -80.0f;
        }
        mixer.SetFloat("MasterVolume", dbVolume);
    }

    public void SetBGMVolume(float volume)
    {
        float dbVolume = Mathf.Log10(volume) * 20;
        if (volume == 0.0f)
        {
            dbVolume = -80.0f;
        }
        mixer.SetFloat("BGMVolume", dbVolume);
    }

    public void SetSFXVolume(float volume)
    {
        float dbVolume = Mathf.Log10(volume) * 20;
        if (volume == 0.0f)
        {
            dbVolume = -80.0f;
        }
        mixer.SetFloat("SFXVolume", dbVolume);
    }
}
