﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class LevelLoader : MonoBehaviour {

    public GameObject loadingScreen;
    public Slider slider;
    public Text progressText;
    private static LevelLoader instance;

    void Awake()
    {
        instance = this;
    }

    public static LevelLoader Get()
    {
        return instance;
    }

    public void LoadLevel(int sceneIndex)
    {
        StartCoroutine(LoadCoroutine(sceneIndex));
        
    }

    IEnumerator LoadCoroutine(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        loadingScreen.SetActive(true);
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            slider.value = progress;
            progressText.text = progress * 100f + " %";
            Debug.Log(progress);
            yield return null;
        }
    }

    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("You Quit");
    }
}
