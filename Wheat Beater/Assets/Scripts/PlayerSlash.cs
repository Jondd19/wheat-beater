﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSlash : MonoBehaviour
{

    [HideInInspector] public bool isAttacking=false;

    bool canHit = true;
    public float cooldown = 2.0f;
    bool right = true;
    public Animator anim;
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && canHit == true)
        {
            isAttacking = true;
            //animation is such a joy and a pain...I imagine...
            anim.SetBool("isAttack", isAttacking);
            AudioManager.instance.Play("Lightsword Fast Swing");
            canHit = false;
            Invoke("CooledDown", cooldown);
        }

        anim.SetBool("isAttack", isAttacking);

        if(Input.GetKeyDown(KeyCode.Q))
        {
            LevelLoader.Get().LoadLevel(0);
        }
    }

    void CooledDown()
    {
        canHit = true;
        if (right == true)
        {
            //GameObject.Find("Player").transform.GetChild(1).gameObject.transform.Translate(.6f, .6f, 0);
            right = false;
        }
        else
        {
            //GameObject.Find("Player").transform.GetChild(1).gameObject.transform.Translate(-.6f, -.6f, 0);
            right = true;
        }
        isAttacking = false;
    }
        
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy") && isAttacking ==true)
        {
            other.GetComponent<EnemyBasic>().health--;
        }
    }

}
