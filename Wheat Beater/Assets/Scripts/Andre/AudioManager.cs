﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour {

    public Sound[] sounds;
    public static AudioManager instance;


    void Awake()
    {
        instance = this;

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.outputAudioMixerGroup = s.mixerGroup;

            s.source.priority = (int) s.priority;
            

        }
    }

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound=> sound.name == name);
        s.source.Play();
    }

    public static AudioManager Get()
    {
        return instance;
    }

}
