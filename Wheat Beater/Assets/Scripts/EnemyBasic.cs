﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyBasic : MonoBehaviour
{
    
    [HideInInspector]public int ScreamCount = 0;
    public Transform target;

    public float speed;

    public float stopdist;

    public float health;

    public bool knight;

    public GameObject Fireball;

    public float waypointDelay = 2;

    public GameObject DeathEffect;

    

    private float timeLeft = 2;

    void Start()
    {

        if (!knight)
        {
            target = GameObject.Find("Egg").transform;

            speed = 5;
        }
        else
        {
            target = GameObject.Find("Player").transform;

            speed = 2;
        }

    }


    private void OnTriggerStay(Collider other)
    {
        if (other == GameObject.Find("Player").GetComponent<CapsuleCollider>())
        {
            if (timeLeft <= -waypointDelay)
            {
                timeLeft = waypointDelay;
            }
            if (timeLeft <= 0)
            {



                AudioManager.instance.Play("Sword Hit 2");
                ScreamCount = ScreamCount + 1;
                //GameObject.Find("Egg").GetComponent<Egghealth>().PlayerHealth -= 10;
                GameManager.Get().PlayerHealth -= 10;
                timeLeft = waypointDelay;
                if(ScreamCount ==4)
                {
                    AudioManager.instance.Play("Alien Scream");
                    ScreamCount = 0;
                }
            }
        }
    }



    // Update is called once per frame
    void Update()
    {

        if (health <= 0)
        {
            //if (GameObject.Find("Egg").GetComponent<Egghealth>().enemiesded == GameObject.Find("Egg").GetComponent<Egghealth>().spawny-1)
            if(GameManager.Get().enemiesded == GameManager.Get().spawny-1)
            {
                //GameObject.Find("Egg").GetComponent<Egghealth>().enemiesded += 2;
                GameManager.Get().enemiesded += 2;
            }
            else
            {
                //GameObject.Find("Egg").GetComponent<Egghealth>().enemiesded++;
                GameManager.Get().enemiesded++;
            }
            DeathEffect = Instantiate(DeathEffect, gameObject.transform.position, gameObject.transform.rotation);
            Destroy(gameObject);
        }

        if (target != null)
        {
            if (HasReachedTarget())
            {
                speed = 0;
            }
            
            else{
                speed = 3;
            }
            UpdateMovement();
        }

        
            timeLeft -= Time.smoothDeltaTime;
if (!knight)
        {
            if (timeLeft <= -waypointDelay)
            {
                timeLeft = waypointDelay;
            }


            if (timeLeft <= 0)
            {
                if (Vector3.Distance(transform.position, target.position) <= 80)
                {
                    GameObject Enemo = Fireball;

                    AudioManager.instance.Play("Spell Fire 1");
                    Instantiate(Enemo, transform.position, Quaternion.identity);
                    timeLeft = waypointDelay;
                }
            }
        }

    }




    bool HasReachedTarget()
    {
        float dist = Vector3.Distance(transform.position, target.position);
        return dist <= stopdist;
    }

    void UpdateMovement()
    {
        if (speed > 0)
        {
            Vector3 pos = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
            transform.position = pos;

            Vector3 dir = (target.position) - transform.position;
            if (dir.magnitude > 0)
            {
                Quaternion toRotation = Quaternion.LookRotation(dir);
                transform.rotation = Quaternion.Lerp(transform.rotation, toRotation, 0.1f);
            }
        }
    }

}
   
   

        
       

