﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public GameObject player;
    public int egghealth;
    public int PlayerHealth;
    public Text waveText;
    public GameObject winText;
    public GameObject loseText;
    public GameObject instructionText;
    public int enemiesded;
    [HideInInspector] public int wavenum;
    public int spawny;
    public float cooldown = 2.0f;
    public float countdownTime = 5.0f;
    public float instructionTime = 7.0f;
    public float loseTime = 5.0f;

    private static GameManager instance;

    void Awake()
    {
        instance = this;
    }

   
    public static GameManager Get()
    {
        return instance;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        PlayerHealth = 100;
        egghealth = 100;
        enemiesded = 0;
        wavenum = 1;
        spawny = 0;

        SetWaveText();
        instructionText.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        GameObject.Find("HealthSlider").GetComponent<Slider>().value = PlayerHealth;
        GameObject.Find("EggSlider").GetComponent<Slider>().value = egghealth;

        if (PlayerHealth <= 0)
        {
            Destroy(player);
            GameLose();
        }

        if (egghealth <= 0)
        {
            Destroy(player);
            GameLose();
        }

        if (enemiesded > spawny && wavenum % 2 != 0)
        {

            spawny = 0;
            wavenum++;
            PlayerHealth = 100;
            enemiesded = 0;
            SetWaveText();

        }

        if (enemiesded > spawny && wavenum % 2 == 0)
        {

            spawny = 0;
            wavenum++;
            PlayerHealth = 100;
            enemiesded = 0;
            SetWaveText();

        }

        if (wavenum == 10)
        {
            GameWin();
        }

        instructionTime -= Time.deltaTime;
        if (instructionTime <= 0.0f)
        {
            instructionText.SetActive(false);
        }
    }

    void GameWin()
    {
        winText.SetActive(true);

        countdownTime -= Time.deltaTime;

        if (countdownTime <= 0.0f)
        {
            timerEnded();
            //SceneManager.LoadScene(0);
            LevelLoader.Get().LoadLevel(0);
        }
    }

    void GameLose()
    {
        loseText.SetActive(true);

        loseTime -= Time.deltaTime;

        if (loseTime <= 0.0f)
        {
            loseText.SetActive(false);
            //SceneManager.LoadScene(0);
            LevelLoader.Get().LoadLevel(0);
        }


    }

    void SetWaveText()
    {
        waveText.text = "WAVE: " + wavenum.ToString();
    }

    void timerEnded()
    {
        winText.SetActive(false);
    }

}
